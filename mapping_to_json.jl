# Transforms the mapping file from CSV to JSON

using CSV, JSON

"""
Normalizes a given string: removes surrounding whitespace,
turns multiple whitespaces into a single one and capitalizes
the first letter in the string while making every other letter
lower-case.
"""
function normalize(string::String)
    lowercase(join(split(strip(string), isspace), " "))
end

"""
The main routine.
"""
function main()
    # Read in the relation from the CSV file
    file_path = joinpath(@__DIR__, "the_mapping.csv")
    csv_file  = CSV.File(file_path, header=true, delim="|")
    # Read the file into a hash table of hash tables
    dictionary = Dict()
    for row ∈ csv_file
        skill = normalize(row[1])
        category = normalize(row[2])
        subcategory = normalize(row[3])
        dictionary[skill] = Dict("category" => category, "sub-category" => subcategory)
    end
    # Write dictionary to JSON and write to file
    csv_as_json = JSON.json(dictionary, 4)
    open(joinpath(@__DIR__, "the_mapping.json"),"w") do f
        write(f, csv_as_json)
    end
end

main()
